![Odoo Version](https://img.shields.io/badge/Odoo%20Version-10.0-orange.svg?style=plastic)

# Odoo - Sale International Allowance

## Summary
This demo module add fields to Odoo to set international allowance in Sale orders and Invoices.

## Description
This module adds functioanlity to set a international allowance to sale orders and invoices by percentage or amount. The International allowance is calculated before taxes to the amount_untaxed field, finally printed in the electronic invoice.