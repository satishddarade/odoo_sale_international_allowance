# -*- coding: utf-8 -*-
{
    'name': 'Sale International Allowance',
    'description': """
        Add international allowance to sales flow""",
    'version': '1',
    'license': 'abc.com',
    'author': 'Satish Darade',
    'website': 'www.odoo.com',
    'depends': [
        'sale',
        'account',
    ],
    'data': [
        'views/invoice_international_allowance.xml',
        'security/sale_order_allowance.xml',
        'views/sale_order_allowance.xml',
    ],
    'demo': [
    ],
}
