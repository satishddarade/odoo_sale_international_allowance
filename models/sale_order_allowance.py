# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class Sale_order_allowance(models.Model):

    _description = 'Sale order international allowance'
    _inherit = 'sale.order'

    allowance_type = fields.Selection(
                                     [('percent','Percentage'),
                                      ('amount','Amount')],
                                      string = 'Allowance Type',
                                      help = 'Select allowance type',
                                      default = 'percent')
    allowance_rate = fields.Float( string = 'Allowance Rate', default = '0.0', store = True)

    amount_allowance = fields.Float( string = 'Total International Allowance', compute='_compute_allowance', store = True)


    @api.one
    @api.depends('allowance_type','allowance_rate','amount_total')
    def _compute_allowance(self):

        mod_obj = self.env['ir.model.data']
        amount_allowance = 0.0
        if self.allowance_type == 'percent':
            amount_allowance = self.amount_untaxed * self.allowance_rate / 100
        else:
            amount_allowance = self.allowance_rate

        self.amount_allowance = amount_allowance



    @api.depends('order_line.price_total')
    @api.multi
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """

        for order in self:
            amount_untaxed = amount_tax = amount_allowance = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            if self.allowance_type == 'percent':
                amount_allowance = amount_untaxed * self.allowance_rate / 100
            else:
                amount_allowance = self.allowance_rate

            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_allowance': order.pricelist_id.currency_id.round(amount_allowance),
                'amount_total': amount_untaxed - amount_allowance + amount_tax,
            })



    @api.model
    def _prepare_invoice(self):
        """
        This method send the allowance_type, allowance_rate and amount_allowance to the
        account.invoice model
        """
        res = super(Sale_order_allowance, self)._prepare_invoice()
        res['allowance_type'] = self.allowance_type
        res['allowance_rate'] = self.allowance_rate

        return res

